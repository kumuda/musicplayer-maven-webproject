Description:  An on-line song library where a user can store favorites. The main objective of this project was to put original track,karaoke ,lyrics and song information all in one place. Features like add, edit, search, delete are present.this is a basic version which includes only CRUD operations.   
 The goal for next versions will be to fulfill the other objectives of this project, Which includes gathering of karaokesand lyrics of the songs under the same heading. And implementing a player in the project where user can listen to songs.    
Technologies:  HTML5,jQuery,CSS3,Bootstrap 3,Spring-3.0.5, Hibernate,Maven,Apache Ant,
MySQL-4.14, SWING, Apache Tika-1.5, JUnit-4.11, DbUnit-2.4.9, Selenium-2.25, Apache Tomcat-7.0.55, JavaScript.
Version 1.0