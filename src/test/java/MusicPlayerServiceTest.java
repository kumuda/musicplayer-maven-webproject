/*import java.sql.SQLException;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.validation.constraints.AssertTrue;

import model.Album;
import model.Genre;
import model.Library;
import model.Song;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import service.MusicPlayerDao;
import service.MusicPlayerService;
import service.MusicPlayerServiceImpl;

@RunWith(JMock.class)
public class MusicPlayerServiceTest {

	Mockery context = new Mockery();
	MusicPlayerServiceImpl service;
	Song song;
	MusicPlayerDao dao;
	TransactionManager txn;

	@Before
	public void setUp() {

		service = (MusicPlayerServiceImpl) MusicPlayerServiceImpl.getInstance();
		dao = context.mock(MusicPlayerDao.class);
		service.setMusicPlayerDao(dao);
		txn = context.mock(TransactionManager.class);
		service.setTransactionManager(txn);
	}

	@Test
	public void getSongTest() throws Exception {
		song = new Song("title", null, 3, "artist", "composer", null);
		context.checking(new Expectations() {
			{
				oneOf(txn).begin();
			}
		});

		context.checking(new Expectations() {
			{
				oneOf(txn).commit();
			}
		});

		context.checking(new Expectations() {
			{
				oneOf(dao).getSong("title");
				will(returnValue(song));
			}
		});

		System.out.println("song title:" + song.getTitle());
		Song returnSong = service.getSong("title");
		System.out.println(returnSong.getTitle());
		System.out.println(returnSong.getAlbum());
		System.out.println(returnSong.getRating());
		System.out.println(song.equals(returnSong));
		assertEquals(song, returnSong);

	}

	@Test
	public void getSongFailTest() {

		try {
			context.checking(new Expectations() {
				{
					oneOf(txn).begin();
					oneOf(txn).rollback();
				}
			});
		} catch (NotSupportedException | SystemException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
			context.checking(new Expectations() {
				{
					allowing(dao).getSong("title");
					will(throwException(new SQLException()));
				}
			});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		boolean exceptionThrown = false;
		try {
			service.getSong("title");
		} catch (SQLException e) {
			System.out.println("get song sql exception");
			exceptionThrown = true;
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue("exception thrown must be:" + SQLException.class,
				exceptionThrown);

	}

	@Test
	public void getsongNullTest() throws Exception {

		Song returnSong = service.getSong(null);
		assertEquals(null, returnSong);
	}

	@Test
	public void addSongTest() throws Exception {
		Album album = new Album();
		album.setAlbumName("album");
		Genre genre = new Genre();
		genre.setGenreName("genre");
		song = new Song("title", album, 3, "artist", "composer", genre);
		context.checking(new Expectations() {
			{
				oneOf(txn).begin();
				oneOf(txn).commit();
			}
		});

		context.checking(new Expectations() {
			{
				oneOf(dao).addSong(song, 1);
				will(returnValue(true));

			}
		});
		boolean status = service.addSong(song, 1);
		context.assertIsSatisfied();
	}

	@Test
	public void addSongNullTest() throws Exception {
		song = null;
		boolean status = service.addSong(song, 1);
		assertEquals(false, status);

	}

	@Test
	public void addSongTitleNullTest() throws Exception {
		song = new Song(null, null, 3, "artist", "composer", null);
		boolean status = service.addSong(song, 1);
		assertEquals(false, status);
	}

	@Test
	public void addSongSQLExceptionTest() {
		Album album = new Album();
		album.setAlbumName("album");
		Genre genre = new Genre();
		genre.setGenreName("genre");
		song = new Song("title", album, 3, "artist", "composer", genre);
		try {
			context.checking(new Expectations() {
				{
					oneOf(txn).begin();
					oneOf(txn).rollback();
				}
			});

			context.checking(new Expectations() {
				{
					oneOf(dao).addSong(song, 1);
					will(throwException(new SQLException()));

				}
			});
		} catch (NotSupportedException | SystemException | SQLException e) {
			e.printStackTrace();
		}

		boolean exceptionThrown = false;
		try {
			service.addSong(song, 1);
		} catch (SQLException e) {
			exceptionThrown = true;
		} catch (Exception e) {

		}
		assertTrue("exception trown should be:" + SQLException.class,
				exceptionThrown);
	}

	@Test
	public void deleteSuccessTest() throws Exception {
		final String title = "title";
		context.checking(new Expectations() {
			{
				oneOf(txn).begin();
				oneOf(txn).commit();

			}
		});

		context.checking(new Expectations() {
			{
				oneOf(dao).deleteSong(title);
				will(returnValue(1));
			}
		});

		int status = service.delete(title);
		assertEquals(1, status);

	}

	@Test
	public void deleteTitleNullTest() throws Exception {
		int status = service.delete(null);
		assertEquals(0, status);
	}

	@Test
	public void deleteSQLExceptionTest() {

		try {
			context.checking(new Expectations() {
				{
					oneOf(txn).begin();
				}
			});

			context.checking(new Expectations() {
				{
					oneOf(dao).deleteSong("title");
					will(throwException(new SQLException()));
				}
			});

		} catch (NotSupportedException | SystemException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean exceptionThrown = false;
		try {
			service.delete("title");
		} catch(SQLException e){
			exceptionThrown = true;
			System.out.println("delete sqlexception");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue("exception thrown should be:"+SQLException.class,exceptionThrown);
	}
	
	@Test
	public void listAllSongsSuccessTest() throws Exception{
		Song songOne = new Song("titleOne",null,4,"artistOne","composerOne",null);
		Song songTwo = new Song("titleTwo",null,4,"artistTwo","composerTwo",null);
		final Library library = new Library();
		library.add(songOne);
		library.add(songTwo);
		context.checking(new Expectations() {
			{
				oneOf(txn).begin();
				oneOf(txn).commit();
				oneOf(dao).listAllSongs(1);
				will(returnValue(library));
			}
		});
		
		Library returnLibrary = service.listAllSongs(1);
		
		assertEquals(library,returnLibrary);
		
	}
	
	@Test
	public void listAllSongsSQLExceptionTest(){
		Song songOne = new Song("titleOne",null,4,"artistOne","composerOne",null);
		Song songTwo = new Song("titleTwo",null,4,"artistTwo","composerTwo",null);
		final Library library = new Library();
		library.add(songOne);
		library.add(songTwo);
		try {
			context.checking(new Expectations() {
				{
					oneOf(txn).begin();
					oneOf(dao).listAllSongs(1);
					will(throwException(new SQLException()));
				}
			});
		} catch (NotSupportedException | SystemException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean exceptionThrown = false;
		try {
			service.listAllSongs(1);
		} catch (SQLException e) {
			System.out.println("inside list all songs sql exception");
			exceptionThrown = true;
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue("exception thrown should be:"+SQLException.class,exceptionThrown);
	}
	
	@Test
	public void listAllSongsNoUserIdTest() throws Exception{
		Library library = service.listAllSongs(0);
		assertEquals(null,library);
	}
	
	@Test
	public void searchFromtitleSuccessTest() throws NotSupportedException, SystemException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SQLException{
		final String title = "title";
		Song songOne = new Song("titleOne",null,4,"artistOne","composerOne",null);
		Song songTwo = new Song("titleTwo",null,4,"artistTwo","composerTwo",null);
		final Library library = new Library();
		library.add(songOne);
		library.add(songTwo);
		context.checking(new Expectations() {
			{
				oneOf(txn).begin();
				oneOf(txn).commit();
				oneOf(dao).searchFromTitle(title, 1);
				will(returnValue(library));
			}
		});
		
		Library returnLibrary = service.searchFromTitle(title, 1);
		assertEquals(library,returnLibrary);
	}
	
	@Test
	public void searchFromTitleNullTest() throws SQLException, SystemException{
		Library returnLibrary = service.searchFromTitle(null, 0);
		assertEquals(null,returnLibrary);
	}
}
*/