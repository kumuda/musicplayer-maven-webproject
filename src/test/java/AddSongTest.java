/*import static org.junit.Assert.assertEquals;

import javax.transaction.TransactionManager;

import model.Album;
import model.Genre;
import model.Song;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import service.MusicPlayerDao;
import service.MusicPlayerDaoImpl;
import service.MusicPlayerService;
import service.MusicPlayerServiceImpl;
import service.TransactionManagerImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MusicPlayerDaoImpl.class)
public class AddSongTest {

	private MusicPlayerDao dao;
	private MusicPlayerService service;
	private TransactionManager transactionManager;
	Album album;
	Genre genre;
	Song song;

	@Before
	public void setUp() {
		
	}

	private void addSongSetup() throws Exception {
		System.out.println("Add song setup");
		this.service = MusicPlayerServiceImpl.getInstance();
		this.dao = EasyMock.createMock(MusicPlayerDao.class);
		System.out.println(this.dao);
		((MusicPlayerServiceImpl)this.service).setMusicPlayerDao(this.dao);
		this.album = new Album();
		album.setAlbumName("album");
		this.genre = new Genre();
		genre.setGenreName("genre");
		this.song = new Song("song", this.album, 3, "artist", "composer",
				this.genre);
		this.transactionManager = EasyMock.createMock(TransactionManager.class);
		TransactionManagerImpl.setInstance(this.transactionManager);
		//this.transactionManager.rollback();
		//EasyMock.expectLastCall();
		EasyMock.expect(this.dao.addSong(this.song)).andReturn(1);
		//EasyMock.expect(this.dao.addGenre(genre, 1)).andReturn(1);
		//EasyMock.expect(this.dao.addAlbum(album, 1)).andReturn(1);
		//EasyMock.expect(this.dao.userIdSongsMapping(1, 1)).andReturn(1);
		this.transactionManager.begin();
		EasyMock.expectLastCall();
		this.transactionManager.commit();
		EasyMock.expectLastCall();
		EasyMock.replay(this.transactionManager);
		EasyMock.replay(this.dao);
	}

	@Test
	public void addSongs() {
		boolean status = false;
		try {
			this.addSongSetup();
			System.out.println(this.dao.getClass());
			status = this.service.addSong(this.song, 1);
			System.out.println(status);
			assertEquals(true, status);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void nullSong() throws Exception {
		Song song = null;

		this.addSongSetup();

	}
}
*/