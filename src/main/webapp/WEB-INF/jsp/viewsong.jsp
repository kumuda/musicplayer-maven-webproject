<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.Song"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Song</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css.map">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<script src="javascript/jquery.js"></script>
<script src="javascript/bootstrap.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#edit').click(
								function() {
									$('form').find('input,select').removeAttr(
											'disabled');
									$('#edit').replaceWith($('#save'));
									$('#saveDiv').show();
								});

						$('#save')
								.click(
										function() {
											var title = $('#title').val();
											var album = $('#album').val();
											var artist = $('#artist').val();
											var rating = $('#rating').val();
											var composer = $('#composer').val();
											var genre = $('#genre').val();
											var songId = $('#songId').val();

											window.location
													.assign("/MusicPlayer/updateSong?title="
															+ title
															+ "&album="
															+ album
															+ "&artist="
															+ artist
															+ "&rating="
															+ rating
															+ "&composer="
															+ composer
															+ "&genre="
															+ genre
															+ "&songId="
															+ songId);

										});

					});
</script>

</head>
<body>
<body>
	<div class="preview__header">
		<div class="preview__envato-logo">
			<h4>MusicPlayer</h4>
		</div>
		<div class="container">
			<ul class="nav navbar-nav navbar-right">
				<div class="row">
					<div class="col-mod-6">
						<span class="pull-right"><a href="logoutServlet"
							style="color: black">Logout </a> </span>
					</div>
				</div>
			</ul>
		</div>
	</div>

	<div class="spacer100"></div>

	<%
		Song song =  (Song) request.getAttribute("song");
	//	System.out.println(song.getSongId());
	%>
	<input type="hidden" id="songId" value= "<%=song.getSongId() %>"/>
	<div class="container" align="center">
		<div class="container" align="center">
			<form role="form" name="view" class="form-horizontal" align="left">
				<div class="form-group row">
					<div class="col-md-1 col-md-offset-4">
						<label for="title">Title</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="title"
							value="<%=song.getTitle()%>" name="title" disabled />

					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-1 col-md-offset-4">
						<label for="album">Album</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="album"
							value="<%= song.getAlbum()%>" name="album" disabled />

					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-1 col-md-offset-4">
						<label for="album">Artists</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="artist"
							value="<%= song.getArtist()%>" name="artist" disabled />
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-1 col-md-offset-4">
						<label for="album">Rating</label>
					</div>
					<div class="col-md-1">
						<select id="rating" class="form-control" disabled>
							<option value="<%= song.getRating()%>"><%=  song.getRating()%></option>
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-1 col-md-offset-4">
						<label for="album">Composer</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="composer"
							value="<%= song.getComposer()%>" name="composer" disabled />

					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-1 col-md-offset-4">
						<label for="album">Genre</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="genre"
							value="<%= song.getGenre()%>" name="genre" disabled />

					</div>
				</div>
			</form>
		</div>



		<div align="center" id="editDiv">
			<input type="submit" name="edit" value="edit" id="edit"
				class="btn btn-primary"> <a class="btn btn-default"
				href="home">Close</a>
		</div>

		<div align="center" id="saveDiv" hidden>
			<input type="submit" name="save" value="save" id="save"
				class="btn btn-primary">
		</div>
	</div>
</body>
</html>