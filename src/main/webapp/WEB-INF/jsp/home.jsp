<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.Library"%>
<%@ page import="model.Song"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MusicPlayer</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<body>
	<div class="preview__header">
		<div class="preview__envato-logo">
			<h4>MusicPlayer</h4>
		</div>
		<div class="container">
			<ul class="nav navbar-nav navbar-right">
				<div class="row">
					<div class="col-mod-6">
						<span class="pull-right"><a href="logoutServlet"
							style="color: black">Logout </a> </span>
					</div>
				</div>
			</ul>
		</div>
	</div>

	<div class="spacer100"></div>
	<div class="container">
		<div align="left" width="100%">
			<button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#myModal" name="Add" value="Add">
				<span class="glyphicon glyphicon-plus"></span>
			</button>
			<!-- <input type="submit" width="50" name="Add" value="Add" class=" btn btn-primary" data-toggle="modal" data-target="#myModal" /> -->
			<button type="button" name="Delete" id="delete"
				class=" btn btn-primary">
				<span class="glyphicon glyphicon-trash"></span>
			</button>
			<!-- 
<input type="submit" width="50" name="Delete" value="Delete" id="delete" class=" btn btn-primary"/> -->
			<span class="pull-right"> <input type="text" width="150"
				name="Search" placeholder="search title" id="search"
				class="form-control focusedInput" />
			</span>
			<!-- <input type="text"  width="150" name="Search" placeholder="search" id="search" class=" btn btn-default"/>   -->
			<table class="table" id="table" border="0">
				<thead>
					<tr>
						<th>Select</th>
						<th>Title</th>
						<th>Album</th>
						<th>Artists</th>
						<th>Rating</th>
						<th>Composer</th>
						<th>Genre</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Add a Song</h4>
				</div>
				<div class="modal-body">
					<div style="position: relative;">
						<form action="addSongServlet" method="post"
							enctype="multipart/form-data">
							<a class='btn btn-primary' href='javascript:;'> Choose
								File... <input type="file"
								style='position: absolute; z-index: 2; top: 0; left: 0; filter: alpha(opacity =   0); opacity: 0; background-color: transparent; color: transparent;'
								name="file_source" size="40"
								onchange='$("#upload-file-info").html($(this).val());'>
							</a> &nbsp; <span class='label label-info' id="upload-file-info"></span>
							<br /> <input type="submit" value="Upload"
								class="btn btn-primary" />
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script src="javascript/jquery.js"></script>
	<script src="javascript/bootstrap.js"></script>
	<script src="javascript/home.js"></script>
</body>
</html>