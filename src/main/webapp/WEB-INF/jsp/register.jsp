<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<link rel="stylesheet" type="text/css" href="css/css/bootstrap.css.map">
<script src="javascript/jquery.js"></script>
<script src="javascript/bootstrap.js"></script>
</head>
<body>
	<!-- <div class="container" align="center">
<form class="form-inline" role="form" action="addUser" method="post">
<div class="form-group">
<label for="username">Username</label>
<input type="text" class="form-control focusedInput" id="username" placeholder="username" name="username"/>
</div><br><div class="spacer10"></div>
<div class="form-group">
<label for="password">Password</label>
<input type="password" class="form-control focusedInput" id="password" placeholder="password" name="password"/>
</div><br><div class="spacer10"></div>
<input type="submit" class="btn btn-default" id="signIn" value="Sign In" name="signIn"/>
</form><br><div class="spacer10"></div>
</div> -->
	<div class="preview__header">
		<div class="preview__envato-logo">
			<h4>MusicPlayer</h4>
		</div>
	</div>

	<div class="login_main_cont">
		<div class="login_cont group login">
			<div class="login_form modal-signup">
				<h2>Register</h2>
				<form class=class= "formClass" action="addUser" method="post">
					<fieldset>
						<input type="text" class="form-control focusedInput" id="username"
							placeholder="username" name="username" required />
					</fieldset>
					<div class="spacer10"></div>
					<fieldset>
						<input type="password" class="form-control focusedInput"
							id="password" placeholder="password" name="password" required />
					</fieldset>
					<div class="spacer10"></div>
					<fieldset class="login_submit">
						<button type="submit" class="buton">SIGN UP</button>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>