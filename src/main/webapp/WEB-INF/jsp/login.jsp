<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css.map">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<script src="javascript/jquery.js"></script>
<script src="javascript/bootstrap.js"></script>
</head>
<body>
	<div class="preview__header">
		<div class="preview__envato-logo">
			<h4>MusicPlayer</h4>
		</div>
	</div>

	<div class="login_main_cont">
		<div class="login_cont group login">
				<div class="login_form modal-signup">
					<h2>Login</h2>
					<form method="post" name="login" action="<c:url value='j_spring_security_check'/>" 
						class="formClass">
						<fieldset>
							<input type="text" class="form-control focusedInput"
								id="username" placeholder="username" name='j_username' required />
						</fieldset>
						<div class="spacer10"></div>
						<fieldset>
							<input type="password" class="form-control focusedInput"
								id="loginPassword" placeholder="password" name='j_password'
								required />
						</fieldset>
						<div class="spacer10"></div>
						<fieldset class="login_submit">
							<button type="submit" class="buton">SIGN IN</button>
						</fieldset>
					</form>
					<div class="spacer10"></div>
					<a class="link-sign" href="register" align="center" >Don't have
						an account?&nbsp;Sign up!</a>
				</div>
		</div>
	</div>

	<!-- <div class="container" align="center">
<h4><strong>MusicPlayer</strong></h4>
<form role="form" method="post" name="login" action="loginServlet" class="form-inline" >
<div class="form-group">
<label for="username">Username</label>
<input type="text" class="form-control focusedInput" id="username" placeholder="username" name="username"/>
</div><br><div class="spacer10"></div>
<div class="form-group">
<label for="password">Password</label>
<input type="password" class="form-control focusedInput" id="loginPassword" placeholder="password" name="password" />
</div><br><div class="spacer10"></div>
<input type="submit" class="btn btn-default" id="login" value="Login" />
</form><div class="spacer10"></div>

 <a href="register" class="btn btn-default" role="button">Register
   </a> -->
	</div>

	<!-- <div class="w-container center">
		<div class="modal-signup">
			<div class="signup-form">
				<form method="post" name="login" action="loginServlet"
					class="form-inline">
					<input type="text" class="w-input singup-field" id="username"
						placeholder="username" name="username" required /> <input
						type="password" class="w-input singup-field" id="password"
						placeholder="password" name="password" required /> <input
						class="w-button notify-btn sign-btn" type="submit"
						value="sign in!" data-wait="Please wait..." id="login">
				</form>
			</div>
		</div> 
	</div>-->
</body>
</html>
