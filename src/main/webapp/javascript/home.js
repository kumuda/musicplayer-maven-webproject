$(document).ready(function(){

	
	$('#delete').on('click',function(){
		var songTitleArray = [];
		$('.checkbox:checked').each(function(){
			songTitleArray.push($(this).val());
		});
			window.location.assign("/MusicPlayer/deleteSong?songTitleArray="+songTitleArray);
	});
	
	
	$('#table').hover(function(){
		$('#table').css({'cursor':'pointer'});
	});
	
	
	$("#logout").click(function(){
		window.location.assign("/MusicPlayer/logoutServlet");
	});
	
	$(window).load(function(){
		$.get("/MusicPlayer/getSongList", function(data){
			 var library = data;
			 listSongs(library);
		});
	}); 
	
	
 function viewFunction(){
	$('#table tbody').on('click','tr td:not(.song-select)',function () {
		var songTitle = $(this).parent().children('td:eq(1)').text();
		window.location.assign("/MusicPlayer/getSong?songTitle="+songTitle);
	});
 }

 function listSongs(library){
	 var table = $("#table");
	 var row;
	 $("#table tbody").remove();
	 $.parseJSON(library).forEach(function(song){
		 row = $("<tr></tr>");
		 	$("<td />").addClass("song-select").append($("<input />").attr({type:"checkbox",class:"checkbox",value:song.title})).appendTo(row);
			$("<td>"+song.title+"</td>").appendTo(row);
			$("<td>"+song.album+"</td>").appendTo(row);
			$("<td>"+song.artist+"</td>").appendTo(row);
			$("<td>"+song.rating+"</td>").appendTo(row);
			$("<td>"+song.composer+"</td>").appendTo(row);
			$("<td>"+song.genre+"</td>").appendTo(row);
			row.click(viewFunction());
			row.appendTo(table);
	 });
 }
 
 $("#search").keypress(function(){
	 	var title = $("#search").val();
	 	$.get("/MusicPlayer/searchSongByTitle?title=" + title, function(data){
			 var library = data;
			 listSongs(library);
		});
	});
});
 