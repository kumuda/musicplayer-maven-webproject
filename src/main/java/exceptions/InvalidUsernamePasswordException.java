package exceptions;

public class InvalidUsernamePasswordException extends Exception{
	
	private String message;
	
	public InvalidUsernamePasswordException(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}

}
