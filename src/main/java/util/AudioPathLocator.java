package util;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;

public class AudioPathLocator extends SimpleFileVisitor<Path> {
	private static ArrayList<Path> paths;

	public AudioPathLocator(ArrayList<Path> paths) {
		AudioPathLocator.paths = paths;
	}

	public AudioPathLocator() {

	}

	public FileVisitResult visitFile(Path visitedPath, BasicFileAttributes attr)
			throws IOException, StringIndexOutOfBoundsException {

		if (Files.probeContentType(visitedPath).equals("audio/mpeg")
				|| Files.probeContentType(visitedPath).equals("audio/x-wav")
				|| Files.probeContentType(visitedPath).equals("audio/x-ms-wma")) {

			paths.add(visitedPath);
		}

		return FileVisitResult.CONTINUE;

	}

	public FileVisitResult visitFileFailed(Path visitedPath, IOException e)
			throws IOException {
		System.err.printf("Visiting failed for %s\n", visitedPath);

		return FileVisitResult.SKIP_SUBTREE;
	}

	public ArrayList<Path> getSongsLocation(Path baseDir) throws IOException,
			StringIndexOutOfBoundsException {
		ArrayList<Path> fileList = new ArrayList<Path>();
		FileVisitor<Path> fileCollector = new AudioPathLocator(fileList);
		Files.walkFileTree(baseDir, fileCollector);
		return paths;

	}
}
