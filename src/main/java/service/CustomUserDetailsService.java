package service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.Login;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * A custom service for retrieving users from a custom datasource, such as a database.
 * <p>
* This custom service must implement Spring's {@link UserDetailsService}
 */
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	MusicPlayerService service;

protected static Logger logger = Logger.getLogger("service");

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		UserDetails user = null;
		try {
			Login dbUser = service.loadUserByUsername(username);
			user = new User(
			dbUser.getUserName(), 
			dbUser.getPassword().toLowerCase(),
			true, true, true, true, getAuthorities(dbUser.getRole().getId()));

		} catch (Exception e) {
			System.out.println("Exception:");
			e.printStackTrace();
			throw new UsernameNotFoundException("Error in retrieving user");
		}
		return user;
	}

	public Collection<GrantedAuthority> getAuthorities(Integer access) {
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);
		logger.debug("Grant ROLE_USER to this user");
		authList.add(new GrantedAuthorityImpl("ROLE_USER"));
//		if ( access.compareTo(1) == 0) {
//
//			logger.debug("Grant ROLE_ADMIN to this user");
//			authList.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
//		}
		return authList;
	}
}