package service;

import model.Role;

public interface RoleService {

	public Role getRole(int id);

}