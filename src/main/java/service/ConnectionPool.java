package service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.transaction.xa.XAResource;

import util.DBConnection;

public class ConnectionPool {

	private static ThreadLocal<XAResource> threadLocal = new ThreadLocal<XAResource>();
	private static ArrayList<DBConnection> connectionList;
	private static ConnectionPool connectionPool;

	private ConnectionPool() {

	}

	public static ConnectionPool getInstance() {
		if (connectionPool == null) {
			connectionPool = new ConnectionPool();
		}
		return connectionPool;
	}

	static {

		try {
			connectionList = new ArrayList<DBConnection>(10);
			for (int count = 0; count < 10; count++) {
				DBConnection connection = new DBConnection();
				connectionList.add(connection);
			}

		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}

	}

	public synchronized XAResource getXAResource() throws SQLException,
			IOException {
		XAResource xaResource = null;

		for (DBConnection connection : connectionList) {
			if (connection.isActive()) {
				connection.setStatus(false);
				xaResource = (XAResource) connection;
				break;
			}

		}
		if (xaResource == null) {
			DBConnection connection = new DBConnection();
			connectionList.add(connection);
			xaResource = (XAResource) connection;
			connection.setStatus(false);
		}
		threadLocal.set(xaResource);
		return xaResource;
	}

	public DBConnection getConnection() {
		DBConnection connection = (DBConnection) threadLocal.get();
		return connection;
	}

}
