package service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

public class TransactionManagerImpl implements TransactionManager {

	private static ThreadLocal<Transaction> threadLocal = new ThreadLocal<Transaction>();

	private static TransactionManager transactionManager;

	public static TransactionManager getInstance() {
		if (transactionManager == null) {
			synchronized (TransactionManagerImpl.class) {
				if (transactionManager == null) {
					transactionManager = new TransactionManagerImpl();
				}

			}
			transactionManager = new TransactionManagerImpl();
		}
		return transactionManager;
	}

	public static void setInstance(TransactionManager manager) {
		transactionManager = manager;
	}

	@Override
	public void begin() throws NotSupportedException, SystemException {

		try {
			ConnectionPool connectionPool = ConnectionPool.getInstance();

			XAResource xaResource = connectionPool.getXAResource();
			Transaction transaction = new TransactionImpl();
			transaction.enlistResource(xaResource);
			threadLocal.set(transaction);
			Connection connection = (Connection) xaResource;
			connection.setAutoCommit(false);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void commit() throws RollbackException, HeuristicMixedException,
			HeuristicRollbackException, SecurityException,
			IllegalStateException, SystemException {
		// TODO Auto-generated method stub
		threadLocal.get().commit();

	}

	@Override
	public int getStatus() throws SystemException {
		// TODO Auto-generated method stubt
		return 0;
	}

	@Override
	public Transaction getTransaction() throws SystemException {
		// TODO Auto-generated method stub

		return threadLocal.get();
	}

	@Override
	public void resume(Transaction tobj) throws InvalidTransactionException,
			IllegalStateException, SystemException {
		// TODO Auto-generated method stub

	}

	@Override
	public void rollback() throws IllegalStateException, SecurityException,
			SystemException {
		// TODO Auto-generated method stub
		threadLocal.get().rollback();

	}

	@Override
	public void setRollbackOnly() throws IllegalStateException, SystemException {

	}

	@Override
	public void setTransactionTimeout(int seconds) throws SystemException {
		// TODO Auto-generated method stub

	}

	@Override
	public Transaction suspend() throws SystemException {
		// TODO Auto-generated method stub
		return null;
	}

}
