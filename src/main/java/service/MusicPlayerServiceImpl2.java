package service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.SystemException;

import model.Library;
import model.Login;
import model.Role;
import model.Song;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jasypt.util.password.StrongPasswordEncryptor;

import util.HibernateUtil;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

public class MusicPlayerServiceImpl2 implements MusicPlayerService {

	private static MusicPlayerServiceImpl2 service;

	public MusicPlayerServiceImpl2() {
		// TODO Auto-generated constructor stub
	}

	public static MusicPlayerService getInstance() {
		if (service == null) {
			service = new MusicPlayerServiceImpl2();
		}
		return service;
	}
	
	

	@Override
	public Song getSong(String title) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Song song = null;

		session.beginTransaction();
		String hql = "select song" + " from Song song"
				+ " join song.genre genre" + " join song.album album"
				+ " where song.title= :title";
		Query query = session.createQuery(hql);
		query.setParameter("title", title);
		List result = query.list();

		if (!result.isEmpty()) {
			for (Object object : result) {
				song = (Song) object;
			}
		}
		return song;
	}

	@Override
	public Library searchFromTitle(String title, int userId)
			throws SQLException, SystemException {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Library library = new Library();

		session.beginTransaction();
		String hql = "select song " + "from Login l join l.songs song "
				+ "join song.genre genre " + "join song.album album "
				+ "where l.userId= :userId " + "and song.title like :title";

		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		query.setParameter("title", "%" + title + "%");
		List result = query.list();

		if (!result.isEmpty()) {
			for (Object object : result) {
				Song song = (Song) object;
				library.add(song);
			}
		}

		return library;
	}

	@Override
	public Library listAllSongs(int userId) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Library library = new Library();

		session.beginTransaction();
		String hql = "select song " + "from Login l join l.songs song "
				+ "join song.genre genre " + "join song.album album "
				+ "where l.userId= :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		List result = query.list();

		if (!result.isEmpty()) {
			for (Object object : result) {
				Song song = (Song) object;
				library.add(song);
			}
		}

		return library;
	}

	@Override
	public boolean update(int songId, Song newSong) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean status = false;
		try {
			session.beginTransaction();
			Song song = (Song) session.get(Song.class, songId);
			song.setTitle(newSong.getTitle());
			song.setAlbum(newSong.getAlbum());
			song.setArtist(newSong.getArtist());
			song.setRating(newSong.getRating());
			song.setComposer(newSong.getComposer());
			song.setGenre(newSong.getGenre());
			session.update(song);
			session.getTransaction().commit();
			status = true;
		} catch (HibernateException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}

		return status;
	}

	@Override
	public boolean delete(String title, int userId) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean status = false;
		try {
			System.out.println("inside delete method of service");
			session.beginTransaction();
			Login login = (Login) session.get(Login.class, userId);
			List<Song> songs = login.getSongs();
			for (int songsIndex=0;songsIndex<songs.size();songsIndex++) {
				if(songs.get(songsIndex).getTitle().equals(title)){
					songs.remove(songsIndex);
					System.out.println("soong removed");
					break;
				}
			}
			login.setSongs(songs);
			session.update(login);
			session.getTransaction().commit();
			status = true;
		} catch (HibernateException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}

		return status;
	}

	@Override
	public Song getSongFromSongId(int songId, int userId) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		return null;
	}

	@Override
	public int addUser(Login login) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		int userId = 0;

		try {
			session.beginTransaction();
			Role role = (Role) session.get(Role.class, 1);
			login.setRole(role);
			userId = (Integer) session.save(login);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
		} finally {
			session.close();
		}

		return userId;
	}

	@Override
	public Login userLogin(Login login)
			throws MySQLIntegrityConstraintViolationException, SQLException,
			Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		int userId = 0;
		Login resultLogin = null;
		boolean isMatch = false;
		String passwordEncrypt = null;
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		session.beginTransaction();
		String hql = "from model.Login log where log.userName= :userName";
		Query query = session.createQuery(hql);
		query.setParameter("userName", login.getUserName());
		for (Iterator it = query.iterate(); it.hasNext();) {
			resultLogin = (Login) it.next();
			userId = resultLogin.getUserId();
			passwordEncrypt = resultLogin.getPassword();
			isMatch = passwordEncryptor.checkPassword(login.getPassword(),
					passwordEncrypt);
		}

		if (!isMatch) {
			resultLogin = null;
		}
		session.getTransaction().commit();
		session.close();

		return resultLogin;
	}

	@Override
	public boolean addSong(Song song, int userId) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean status = false;
		
		System.out.println("inside add song service");
		try {

			session.beginTransaction();
			List<Song> songs = new ArrayList<Song>();
			songs.add(song);
			Login login = (Login) session.get(Login.class, userId);
			login.addSong(song);
			session.saveOrUpdate(song);
			session.getTransaction().commit();
			status = true;
		} catch (HibernateException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return status;
	}

	@Override
	public Login loadUserByUsername(String userName) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		 List<Login> userList = new ArrayList<Login>();
	        Query query = session.createQuery("from Login l where l.userName = :userName");
	        query.setParameter("userName", userName);
	        userList = query.list();
	        if (userList.size() > 0)
	            return userList.get(0);
	        else
	            return null;    
	   
	}

	@Override
	public int getUserIdByUsername(String username) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		int userId = 0;
		 
	        Query query = session.createQuery("select l.userId from Login l where l.userName = :userName");
	        query.setParameter("userName", username);
	        List userIdList = query.list();
	        if (userIdList.size() > 0)
	            return (int)userIdList.get(0);
	        else
	            return 0;    
	  
	}

}
