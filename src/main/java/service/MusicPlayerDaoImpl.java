package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import util.DBConnection;

import model.Album;
import model.Genre;
import model.Library;
import model.Song;

public class MusicPlayerDaoImpl implements MusicPlayerDao {

	private static MusicPlayerDaoImpl dao;

	public static void setInstance(MusicPlayerDaoImpl d) {
		dao = d;
	}

	@Override
	public boolean addSong(Song song, int userId) throws SQLException {
		Connection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int songId = 0;
		int genreId = 0;
		int albumId = 0;
		int status = 0;
		// log.info("Database Connection Established in  addsongs  ");

		preparedStatement = connection
				.prepareStatement(
						"insert into songs (title,artists,rating,composer) values (?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, song.getTitle());
		preparedStatement.setString(2, song.getArtist());
		preparedStatement.setInt(3, song.getRating());
		preparedStatement.setString(4, song.getComposer());
		preparedStatement.executeUpdate();
		resultSet = preparedStatement.getGeneratedKeys();
		if (resultSet.next()) {
			songId = resultSet.getInt(1);
		}
		if (song.getGenre() != null && song.getGenre().getGenreName() != null) {
			genreId = this.addGenre(song.getGenre(), songId, connection);
		}
		if (song.getAlbum() != null && song.getAlbum().getAlbumName() != null) {
			albumId = this.addAlbum(song.getAlbum(), songId, connection);
		}
		if (userId != 0 && songId != 0) {
			status = this.userIdSongsMapping(userId, songId, connection);
		}
		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}
		
		if(songId != 0 && status != 0){
			return true;
		}
		return false ;
	}

	// @Override
	private int addGenre(Genre genre, int songId,Connection connection) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int genreId = 0;
		preparedStatement = connection
				.prepareStatement("select * from genre where genre_name = (?)");
		preparedStatement.setString(1, genre.getGenreName());
		resultSet = preparedStatement.executeQuery();
		if (!resultSet.isBeforeFirst()) {
			preparedStatement = connection.prepareStatement(
					"insert into genre (genre_name) values (?)",
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, genre.getGenreName());
			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				genreId = resultSet.getInt(1);
			}
		} else {
			if (resultSet.next()) {
				genreId = resultSet.getInt("genre_id");
			}

		}

		preparedStatement = connection
				.prepareStatement("insert into genre_songs_mapping (song_id,genre_id) values (?,?)");
		preparedStatement.setInt(1, songId);
		preparedStatement.setInt(2, genreId);
		preparedStatement.executeUpdate();

		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}
		return genreId;
	}

	// @Override
	private int addAlbum(Album album, int songId,Connection connection) throws SQLException {
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int albumId = 0;

		preparedStatement = connection
				.prepareStatement("select * from albums where album_name = (?)");
		preparedStatement.setString(1, album.getAlbumName());
		resultSet = preparedStatement.executeQuery();

		if (!resultSet.isBeforeFirst()) {
			preparedStatement = connection.prepareStatement(
					"insert into albums (album_name) values (?) ",
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, album.getAlbumName());
			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				albumId = resultSet.getInt(1);
			}
		} else {
			resultSet.next();
			albumId = resultSet.getInt("album_id");
		}

		preparedStatement = connection
				.prepareStatement("insert into albums_songs_mapping (song_id,album_id) values (?,?)");
		preparedStatement.setInt(1, songId);
		preparedStatement.setInt(2, albumId);
		preparedStatement.executeUpdate();

		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}

		return albumId;
	}

	// @Override
	private int userIdSongsMapping(int userId, int songId,Connection connection) throws SQLException {
		
		PreparedStatement preparedstatement = null;
		int status = 0;
		preparedstatement = connection
				.prepareStatement("insert into user_songs_mapping (user_id,song_id) values (?,?)");
		preparedstatement.setInt(1, userId);
		preparedstatement.setInt(2, songId);
		status = preparedstatement.executeUpdate();

		if (preparedstatement != null) {
			preparedstatement.close();
		}

		return status;
	}

	@Override
	public Library listAllSongs(int userId) throws SQLException {
		Connection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Library library = new Library();

		// log.info("Database Connection Established in  view songs ");

		preparedStatement = connection
				.prepareStatement("select s.song_id,s.title,s.artists,s.rating,s.composer,a.album_name,g.genre_name"
						+ " from songs as s"
						+ " inner join albums_songs_mapping as asm on s.song_id=asm.song_id"
						+ " inner join albums as a on asm.album_id=a.album_id"
						+ " inner join genre_songs_mapping as gsm on s.song_id=gsm.song_id"
						+ " inner join genre as g on gsm.genre_id=g.genre_id"
						+ " inner join user_songs_mapping as usm on s.song_id=usm.song_id where usm.user_id=(?)");
		preparedStatement.setInt(1, userId);
		resultSet = preparedStatement.executeQuery();

		while (resultSet.next()) {

			Song song = new Song();
			Album album = new Album();
			Genre genre = new Genre();

			song.setSongId(resultSet.getInt("song_id"));
			song.setTitle(resultSet.getString("title"));
			album.setAlbumName(resultSet.getString("album_name"));
			song.setArtist(resultSet.getString("artists"));
			song.setRating(resultSet.getInt("rating"));
			song.setComposer(resultSet.getString("composer"));
			genre.setGenreName(resultSet.getString("genre_name"));
			song.setAlbum(album);
			song.setGenre(genre);
			library.add(song);
		}

		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}

		return library;
	}

	@Override
	public Song getSong(String title) throws SQLException {
		DBConnection connection = null;
		System.out.println("After conn");
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Song song = new Song();
		Album album = new Album();
		Genre genre = new Genre();

		System.out.println("inside dao impl getSong method");
		// log.info("this.connection established to view a song.... inside getsong method");
		preparedStatement = connection
				.prepareStatement("select s.song_id,s.title,s.artists,s.rating,s.composer,a.album_name,g.genre_name"
						+ " from songs as s"
						+ " inner join albums_songs_mapping as asm on s.song_id=asm.song_id"
						+ " inner join albums as a on asm.album_id=a.album_id"
						+ " inner join genre_songs_mapping as gsm on s.song_id=gsm.song_id"
						+ " inner join genre as g on gsm.genre_id=g.genre_id"
						+ " where title =(?)");
		preparedStatement.setString(1, title);
		resultSet = preparedStatement.executeQuery();

		if (resultSet.next()) {
			song.setSongId(resultSet.getInt("song_id"));
			song.setTitle(resultSet.getString("title"));
			album.setAlbumName(resultSet.getString("album_name"));
			song.setRating(resultSet.getInt("rating"));
			song.setArtist(resultSet.getString("artists"));
			song.setComposer(resultSet.getString("composer"));
			genre.setGenreName(resultSet.getString("genre_name"));
			song.setAlbum(album);
			song.setGenre(genre);
		}

		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}

		if (resultSet == null) {
			return null;
		} else {
			return song;
		}
	}

	@Override
	public int updateSong(int songId, Song newSong) throws SQLException {
		Connection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		int updateStatus = 0;
		// log.info("this.connection established to update a song");

		preparedStatement = connection
				.prepareStatement("update songs set title=(?),rating=(?),composer=(?),artists=(?) where song_id=(?)");

		preparedStatement.setString(1, newSong.getTitle());
		preparedStatement.setInt(2, newSong.getRating());
		preparedStatement.setString(3, newSong.getComposer());
		preparedStatement.setString(4, newSong.getArtist());
		preparedStatement.setInt(5, songId);
		updateStatus = preparedStatement.executeUpdate();
		int albumId = checkAlbumExistance(newSong.getAlbum(),connection);
		if (albumId == 0 && newSong.getAlbum().getAlbumName() != null) {
			addAlbum(newSong.getAlbum(), songId, connection);
		} else {
			updateAlbum(songId, albumId,connection);

		}
		int genreId = checkGenreExistance(newSong.getGenre(),connection);
		if (genreId == 0) {
			//musicPlayerDao.addGenre(newSong.getGenre(), songId);
		}

		else {
			updateGenre(songId, genreId,connection);
		}

		if (preparedStatement != null) {
			preparedStatement.close();
		}
		return updateStatus;
	}

	//@Override
	private int checkAlbumExistance(Album album,Connection connection) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int albumId = 0;
		preparedStatement = connection
				.prepareStatement("select album_id from albums where album_name=(?)");
		preparedStatement.setString(1, album.getAlbumName());
		resultSet = preparedStatement.executeQuery();

		if (resultSet.next()) {
			albumId = resultSet.getInt("album_id");
		}
		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}
		return albumId;
	}

	//@Override
	private int updateAlbum(int songId, int albumId,Connection connection) throws SQLException {
		PreparedStatement preparedStatement = null;
		int status = 0;
		preparedStatement = connection
				.prepareStatement("update albums_songs_mapping set album_id=(?) where song_id=(?)");
		preparedStatement.setInt(1, albumId);
		preparedStatement.setInt(2, songId);
		status = preparedStatement.executeUpdate();

		if (preparedStatement != null) {
			preparedStatement.close();
		}
		return status;

	}

	//@Override
	private int checkGenreExistance(Genre genre,Connection connection) throws SQLException {
		PreparedStatement preparedstatement = null;
		ResultSet resultSet = null;
		int genreId = 0;
		preparedstatement = connection
				.prepareStatement("select genre_id from genre where genre_name= (?)");
		preparedstatement.setString(1, genre.getGenreName());
		resultSet = preparedstatement.executeQuery();

		if (resultSet.next()) {
			genreId = resultSet.getInt("genre_id");
		}
		return genreId;
	}

	//@Override
	private int updateGenre(int songId, int genreId, Connection connection) throws SQLException {
		PreparedStatement preparedStatement = null;
		int status = 0;
		preparedStatement = connection
				.prepareStatement("update genre_songs_mapping set genre_id=(?) where song_id=(?)");
		preparedStatement.setInt(1, genreId);
		preparedStatement.setInt(2, songId);
		status = preparedStatement.executeUpdate();

		if (preparedStatement != null) {
			preparedStatement.close();
		}

		return status;

	}

	@Override
	public int deleteSong(String title) throws SQLException {
		Connection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int deleteStatus = 0;
		int songId = 0;
		if (title != null) {
			preparedStatement = connection
					.prepareStatement("select song_id from songs where title=(?)");
			preparedStatement.setString(1, title);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				songId = resultSet.getInt("song_id");
			}

			preparedStatement = connection
					.prepareStatement("delete from user_songs_mapping where song_id=(?)");
			preparedStatement.setInt(1, songId);
			deleteStatus = preparedStatement.executeUpdate();
		}
		return deleteStatus;
	}

	@Override
	public Library searchFromTitle(String title, int userId)
			throws SQLException {
		Connection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Library library = new Library();
		preparedStatement = connection
				.prepareStatement("select s.song_id,s.title,s.artists,s.rating,s.composer,a.album_name,g.genre_name"
						+ " from songs as s"
						+ " inner join albums_songs_mapping as asm on s.song_id=asm.song_id"
						+ " inner join albums as a on asm.album_id=a.album_id"
						+ " inner join genre_songs_mapping as gsm on s.song_id=gsm.song_id"
						+ " inner join genre as g on gsm.genre_id=g.genre_id"
						+ " inner join user_songs_mapping as usm on s.song_id=usm.song_id"
						+ "  where title like ? and usm.user_id=(?)");
		preparedStatement.setString(1, "%" + title + "%");
		preparedStatement.setInt(2, userId);
		resultSet = preparedStatement.executeQuery();
		int index = 0;

		while (resultSet.next()) {

			Song song = new Song();
			Album album = new Album();
			Genre genre = new Genre();
			song.setTitle(resultSet.getString("title"));
			album.setAlbumName(resultSet.getString("album_name"));
			song.setArtist(resultSet.getString("artists"));
			song.setRating(resultSet.getInt("rating"));
			song.setComposer(resultSet.getString("composer"));
			genre.setGenreName(resultSet.getString("genre_name"));
			song.setAlbum(album);
			song.setGenre(genre);
			library.add(index, song);
			index++;
		}

		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}
		return library;
	}

	@Override
	public Song getSongFromSongId(int songId, int userId) throws SQLException {
		Connection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Song song = new Song();
		Album album = new Album();
		Genre genre = new Genre();
		preparedStatement = connection
				.prepareStatement("select s.title,s.artists,s.rating,s.composer,a.album_name,g.genre_name"
						+ " from songs as s"
						+ " inner join albums_songs_mapping as asm on s.song_id=asm.song_id"
						+ " inner join albums as a on asm.album_id=a.album_id"
						+ " inner join genre_songs_mapping as gsm on s.song_id=gsm.song_id"
						+ " inner join genre as g on gsm.genre_id=g.genre_id"
						+ " inner join user_songs_mapping on s.song_id=usm.song_id"
						+ " where s.song_id =(?) and usm.user_id=(?)");

		preparedStatement.setInt(1, songId);
		preparedStatement.setInt(2, userId);
		resultSet = preparedStatement.executeQuery();

		resultSet.next();
		song.setTitle(resultSet.getString("title"));
		album.setAlbumName(resultSet.getString("album_name"));
		song.setRating(resultSet.getInt("rating"));
		song.setArtist(resultSet.getString("artists"));
		song.setComposer(resultSet.getString("composer"));
		genre.setGenreName(resultSet.getString("genre_name"));
		song.setAlbum(album);
		song.setGenre(genre);

		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}

		return song;
	}

	public static MusicPlayerDaoImpl getInstance() {
		if (dao == null) {
			synchronized (MusicPlayerDaoImpl.class) {
				if (dao == null) {
					dao = new MusicPlayerDaoImpl();
				}
			}
		}
		return dao;
	}

}
