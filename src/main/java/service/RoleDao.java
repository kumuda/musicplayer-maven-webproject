package service;

import model.Role;

public interface RoleDao {

	public Role getRole(int id);
}
