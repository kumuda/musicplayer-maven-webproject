package service;

import java.sql.SQLException;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.UserTransaction;
import javax.transaction.xa.XAResource;

import util.DBConnection;

import com.mysql.jdbc.Messages;

public class TransactionImpl implements Status, Transaction {

	// private XAResource xaResource;
	@Override
	public void commit() throws RollbackException, HeuristicMixedException,
			HeuristicRollbackException, SecurityException,
			IllegalStateException, SystemException {
		// TODO Auto-generated method stub
		try {
			ConnectionPool.getInstance().getConnection().commit();
			ConnectionPool.getInstance().getConnection().setAutoCommit(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public int getStatus() throws SystemException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void rollback() throws IllegalStateException, SystemException {
		// TODO Auto-generated method stub
		try {
			ConnectionPool.getInstance().getConnection().rollback();
			ConnectionPool.getInstance().getConnection().setAutoCommit(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void setRollbackOnly() throws IllegalStateException, SystemException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean delistResource(XAResource xaRes, int flag)
			throws IllegalStateException, SystemException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void registerSynchronization(Synchronization sync)
			throws RollbackException, IllegalStateException, SystemException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean enlistResource(XAResource xaRes) throws RollbackException,
			IllegalStateException, SystemException {

		// this.xaResource = xaRes;

		return false;
	}

}
