package service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import model.Login;
import model.User;

import org.jasypt.util.password.StrongPasswordEncryptor;

import util.DBConnection;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class MusicPlayerUserDao {
	static Logger log = Logger.getLogger("MusicPlayerUserDao.class");

	private static MusicPlayerUserDao musicPlayerUserDao;

	private MusicPlayerUserDao() {

	}

	public static MusicPlayerUserDao getInstance() {
		if (musicPlayerUserDao == null) {
			musicPlayerUserDao = new MusicPlayerUserDao();
		}
		return musicPlayerUserDao;
	}

	public static void setInstance(MusicPlayerUserDao userDao) {
		musicPlayerUserDao = userDao;
	}

	public int addUsers(Login login)
			throws MySQLIntegrityConstraintViolationException, SQLException {
		DBConnection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int userId = 0;
		preparedStatement = connection.prepareStatement(
				"insert into login (user_name,password) values (?,?)",
				Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, login.getUserName());
		preparedStatement.setString(2, login.getPassword());
		preparedStatement.executeUpdate();

		resultSet = preparedStatement.getGeneratedKeys();
		if (resultSet.next()) {
			userId = resultSet.getInt(1);
		}

		if (preparedStatement != null) {
			preparedStatement.close();
		}

		return userId;
	}

	public int userLogin(Login login) throws SQLException {
		DBConnection connection = null;
		connection = ConnectionPool.getInstance().getConnection();
		System.out.println(connection);
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		int userId = 0;
		String passwordEncrypt = null;
		boolean isMatch = false;
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();

		preparedStatement = connection
				.prepareStatement("select user_id,password from login where user_name=(?)");
		System.out.println("prepared statement");
		System.out.println(preparedStatement);
		preparedStatement.setString(1, login.getUserName());
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			userId = resultSet.getInt("user_id");
			passwordEncrypt = resultSet.getString("password");
			isMatch = passwordEncryptor.checkPassword(login.getPassword(),
					passwordEncrypt);
		}

		if (!isMatch) {
			userId = 0;
		}

		if (resultSet != null) {
			resultSet.close();
		}
		if (preparedStatement != null) {
			preparedStatement.close();
		}
		return userId;
	}

}
