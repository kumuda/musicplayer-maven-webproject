package service;

import java.sql.SQLException;
import model.Library;
import model.Song;

public interface MusicPlayerDao {

	public boolean addSong(Song song,int userId) throws SQLException;

	public Library listAllSongs(int userId) throws SQLException;

	public Song getSong(String title) throws SQLException;

	public int updateSong(int songId, Song newSong) throws SQLException;

	public int deleteSong(String title) throws SQLException;

	public Library searchFromTitle(String title, int userId)
			throws SQLException;

	public Song getSongFromSongId(int songId, int userId) throws SQLException;

}
