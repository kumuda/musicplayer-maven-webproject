package service;

import java.sql.SQLException;

import javax.transaction.SystemException;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import model.Library;
import model.Login;
import model.Song;
import model.User;

public interface MusicPlayerService {
	public boolean addSong(Song song,int userId) throws Exception;

	public Song getSong(String title) throws Exception;

	public Library searchFromTitle(String title, int userId)
			throws SQLException, SystemException;

	public Library listAllSongs(int userId) throws Exception;

	public boolean update(int songId, Song newSong) throws Exception;

	public boolean delete(String title,int userId) throws Exception;

	public Song getSongFromSongId(int songId, int userId) throws Exception;

	public int addUser(Login login) throws Exception;

	public Login userLogin(Login login)
			throws MySQLIntegrityConstraintViolationException, SQLException,
			Exception;
	
	public Login loadUserByUsername(String username);

	public int getUserIdByUsername(String username);
}
