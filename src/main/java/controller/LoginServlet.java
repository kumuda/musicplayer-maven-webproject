package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import model.Login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import exceptions.InvalidUsernamePasswordException;

import service.MusicPlayerService;
import service.MusicPlayerServiceImpl2;

@Controller
public class LoginServlet {

	@Autowired
	MusicPlayerService service;

	@RequestMapping({ "/","/login" })
	public String login() {
		System.out.println("inside login method ");
		return "login";
	}

	/*@RequestMapping({  "/" })
	public String loginInitial() {
		System.out.println("inside login method ");
		return "login";
	}
*/
	
	/*@RequestMapping(value = {"/login"})
	public ModelAndView loginPost(
			HttpSession session,
			@RequestParam(value = "j_username", required = false) String userName,
			@RequestParam(value = "j_password", required = false) String password)
			throws IOException, ServletException {

		try {
			Login login = new Login(userName, password);
			Login returnLogin = service.userLogin(login);
			if (returnLogin.getUserId() != 0) {
				session.setAttribute("user", login.getUserName());
				session.setAttribute("userId", returnLogin.getUserId());

				return new ModelAndView("home");
			} else {
				return new ModelAndView("login", "message",
						"Username or password is wrong");
			}
		}

		catch (InvalidUsernamePasswordException e) {
			return new ModelAndView("login", "message",
					"Username or password is wrong");

		}

		catch (SQLException e) {
			e.printStackTrace();
			return new ModelAndView("login", "message",
					"We are unable to process your request.Please try again ");

		}

		catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("login", "message",
					"We are unable to process your request.Please try again ");
		}

	}*/

}