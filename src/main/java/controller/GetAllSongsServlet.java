package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import model.Library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import service.MusicPlayerService;
import service.MusicPlayerServiceImpl2;

@Controller
public class GetAllSongsServlet {

	@Autowired
	MusicPlayerService service;

	@RequestMapping(value = "/getSongList", method = RequestMethod.GET)
	public @ResponseBody
	String getAllSongs(HttpSession session) {

		Library library = new Library();
		int userId = 0;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		      String username = auth.getName(); 
			System.out.println("inside get songs"+"username==>"+username);
			/*System.out.println(session.getAttribute("userId"));
			System.out.println(session.getAttribute("userId") == null);
			userId = (int) session.getAttribute("userId");*/
			userId = service.getUserIdByUsername(username);
			library = service.listAllSongs(userId);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return library.toString();
	}

	/*
	 * public void doPost(HttpServletRequest request, HttpServletResponse
	 * response) { MusicPlayerService service = MusicPlayerServiceImpl2
	 * .getInstance(); Library library = new Library(); int userId = 0; try {
	 * HttpSession session = request.getSession(); userId = (int)
	 * session.getAttribute("userId"); library = service.listAllSongs(userId);
	 * request.setAttribute("songlibrary", library);
	 * request.setCharacterEncoding("utf8"); //
	 * response.setCharacterEncoding("utf8");
	 * response.setContentType("application/json"); PrintWriter out =
	 * response.getWriter(); JSONObject obj = new JSONObject();
	 * obj.put("library", library); out.print(obj); } catch (SQLException e) {
	 * e.printStackTrace(); } catch (ServletException e) { e.printStackTrace();
	 * } catch (IOException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (Exception e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); } }
	 */
}
