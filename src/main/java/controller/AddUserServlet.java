package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import model.Login;
import model.Role;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import service.MusicPlayerService;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import exceptions.InvalidUsernamePasswordException;


@Controller
public class AddUserServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	MusicPlayerService service;

	@RequestMapping(value = "/register")
	public String register() {
		return "register";
	}
	
	@RequestMapping(value = "/denied")
	public String denied() {
		return "accessdenied";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public ModelAndView addUser(
			HttpSession session,
			@RequestParam(value = "username", required = false) String userName,
			@RequestParam(value = "password", required = false) String tempPassword)
			throws ServletException, IOException {

		/*StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		String password = passwordEncryptor.encryptPassword(tempPassword);*/
		
		Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
		String password = passwordEncoder.encodePassword(tempPassword, null);

		boolean addStatus = false;
		Login login = null;

		int userId = 0;
		try {
			Role role = new Role();
			role.setId(1);
			login = new Login(userName, password,role);
			
		} catch (InvalidUsernamePasswordException e1) {
			return new ModelAndView("register", "message",
					"invalid Username or Password.");
		}

		try {
			userId = service.addUser(login);
			session.setAttribute("user", login.getUserName());
			session.setAttribute("userId", userId);
			return new ModelAndView("home");
		}

		catch (MySQLIntegrityConstraintViolationException e) {
			return new ModelAndView("register", "message",
					"invalid Username or Password.");
		} catch (SQLException e) {
			e.printStackTrace();
			return new ModelAndView("register", "message",
					"invalid Username or Password.");
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("register", "message",
					"invalid Username or Password.");
		}

	}
}
