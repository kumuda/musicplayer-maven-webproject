package controller;

import java.sql.SQLException;

import javax.servlet.http.HttpSession;
import javax.transaction.SystemException;

import model.Library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.MusicPlayerService;
import service.MusicPlayerServiceImpl2;

@Controller
public class SearchSongByTitleServlet {

	@Autowired
	MusicPlayerService service;

	@RequestMapping(value = "/searchSongByTitle", method = RequestMethod.GET)
	public @ResponseBody
	String doGet(HttpSession session,
			@RequestParam(value = "title", required = false) String title) {

		System.out.println("/n/n/n");
		System.out.println("inside service of search song by title");
		Library library = new Library();
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		     String username = auth.getName(); 
			int userId = service.getUserIdByUsername(username);
			library = service.searchFromTitle(title, userId);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		System.out.println(library.toString());
		return library.toString();
	}

	/*
	 * public void doPost(HttpServletRequest request, HttpServletResponse
	 * response) {
	 * 
	 * String title = request.getParameter("songTitle"); MusicPlayerService
	 * service = MusicPlayerServiceImpl2.getInstance(); Library library = new
	 * Library(); try { HttpSession session = request.getSession(); int userId =
	 * (int) session.getAttribute("userId"); library =
	 * service.searchFromTitle(title, userId);
	 * request.setAttribute("songlibrary", library);
	 * request.setCharacterEncoding("utf8"); //
	 * response.setCharacterEncoding("utf8");
	 * response.setContentType("application/json"); PrintWriter out =
	 * response.getWriter(); JSONObject obj = new JSONObject();
	 * obj.put("library", library); out.print(obj); } catch (SQLException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } catch
	 * (ServletException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } catch (SystemException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } }
	 */

}
