package controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import model.Song;

import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

import service.MusicPlayerService;
import service.MusicPlayerServiceImpl2;

@Controller
public class AddSongServlet {

	private static final String UPLOAD_DIR = "uploads";

	@Autowired
	MusicPlayerService service;

	@RequestMapping(value = "/addSongServlet", method = RequestMethod.POST)
	public ModelAndView addSong(
			@RequestParam("file_source") MultipartFile file, HttpSession session)
			throws IOException, ServletException {
		System.out.println("inside add song controller");
		try {

			String rootPath = System.getProperty("catalina.home");
			File fileSaveDir = new File(rootPath + File.separator + UPLOAD_DIR);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			byte[] bytes = file.getBytes();
			File serverFile = new File(fileSaveDir.getAbsolutePath()
					+ File.separator + file.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();

			System.out.println("\n\n\nName:");
			System.out.println(serverFile.getName());
			System.out.println(serverFile.getPath());
			System.out.println("\n\n\n");
			Song song = Song.getSongMetadata(serverFile.getPath());
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		    String username = auth.getName(); 
			int userId = service.getUserIdByUsername(username);
			System.out.println("before add song service");
			service.addSong(song, userId);
			System.out.println("after add song service");
			return new ModelAndView("home");
		} catch (IOException e) {
			e.printStackTrace();
			return new ModelAndView("home", "message", "No file was chosen ");

		}

		catch (SAXException e) {
			e.printStackTrace();
			return new ModelAndView("home", "message",
					"There was some Error while processing.Please Try again");
		} catch (TikaException e) {
			e.printStackTrace();
			return new ModelAndView("home", "message",
					"There was some Error while processing.Please Try again");
		} catch (SQLException e) {
			e.printStackTrace();
			return new ModelAndView("home", "message",
					"There was some Error while processing.Please Try again");
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("home", "message",
					"There was some Error while processing.Please Try again");
		}

	}

	private String getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		System.out.println("content-disposition header= " + contentDisp);
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				return token.substring(token.indexOf("=") + 2,
						token.length() - 1);
			}
		}
		return "";
	}

}
