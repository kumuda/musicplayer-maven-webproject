package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;

import model.Song;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import service.MusicPlayerService;
import service.MusicPlayerServiceImpl2;

@Controller
public class GetSongServlet {

	@Autowired
	MusicPlayerService service;

	@RequestMapping(value = "/getSong", method = RequestMethod.GET)
	public ModelAndView getSong(
			@RequestParam(value = "songTitle", required = false) String title) {

		try {
			Song song = service.getSong(title);
			System.out.println(song.getSongId());
			return new ModelAndView("viewsong", "song", song);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} catch (ServletException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
