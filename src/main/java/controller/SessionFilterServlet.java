package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionFilterServlet implements Filter {

	private ArrayList<String> urlList;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		System.out.println("inside init");
		String urls = filterConfig.getInitParameter("avoid-urls");
		StringTokenizer token = new StringTokenizer(urls, ",");

		urlList = new ArrayList<String>();

		for (String url : urlList) {
			System.out.println(url);
		}

		while (token.hasMoreTokens()) {
			urlList.add(token.nextToken().trim());

		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		System.out.println("inside session filter");

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String url = httpRequest.getServletPath();
		System.out.println("url:...'" + url + "'");
		System.out.println("urlList:..." + urlList);
		System.out.println("urlList.contains(url):..." + urlList.contains(url));
		HttpSession session = httpRequest.getSession(false);
		if (urlList.contains(url)
				|| (session != null && session.getAttribute("user") != null)) {
			
			//.out.println("inside session check condition"+url+session.getAttribute("user"));
			//System.out.println("user session:" + session.getAttribute("user"));
			chain.doFilter(request, response);
		} else {
			httpRequest.getRequestDispatcher("login").forward(httpRequest,
					httpResponse);
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
