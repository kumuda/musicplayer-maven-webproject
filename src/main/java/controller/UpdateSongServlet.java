package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;

import model.Album;
import model.Genre;
import model.Song;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.MusicPlayerService;
import service.MusicPlayerServiceImpl2;

@Controller
public class UpdateSongServlet {

	@Autowired
	MusicPlayerService service;

	@RequestMapping(value = "/updateSong", method = RequestMethod.GET)
	public String updateSong(
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "album", required = false) String albumName,
			@RequestParam(value = "artist", required = false) String artist,
			@RequestParam(value = "rating", required = false) String tempRating,
			@RequestParam(value = "composer", required = false) String composer,
			@RequestParam(value = "genre", required = false) String genreName,
			@RequestParam(value = "songId", required = false) String tempSongId) {

		int rating = Integer.parseInt(tempRating);
		int songId = Integer.parseInt(tempSongId);

		Album album = new Album();
		album.setAlbumName(albumName);

		Genre genre = new Genre();
		genre.setGenreName(genreName);

		Song newSong = new Song(title, album, rating, artist, composer, genre);
		try {
			service.update(songId, newSong);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "home";
	}

}
