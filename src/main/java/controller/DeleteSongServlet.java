package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.MusicPlayerService;

@Controller
public class DeleteSongServlet {

	@Autowired
	MusicPlayerService service;

	@RequestMapping(value = "/deleteSong", method = RequestMethod.GET)
	public String delete(
			HttpSession session,
			@RequestParam(value = "songTitleArray", required = false) String titles) {

		String[] songTitles = titles.split(",");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); 
		int userId = service.getUserIdByUsername(username);

		try {
			for (String title : songTitles) {
				service.delete(title, userId);
				System.out.println("deleted song" + title);
			}
			return "home";
		} catch (SQLException e) {
			e.printStackTrace();
			return "Fail";
		} catch (ServletException e) {
			e.printStackTrace();
			return "Fail";
		} catch (IOException e) {
			e.printStackTrace();
			return "Fail";
		} catch (Exception e) {
			e.printStackTrace();
			return "Fail";
		}

	}
}
