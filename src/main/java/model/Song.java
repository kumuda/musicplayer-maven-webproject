package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

@Entity
@Table(name = "songs")
public class Song implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1910578074414103609L;

	@Id
	@GeneratedValue
	@Column(name = "song_id")
	private int songId;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinTable(name = "albums_songs_mapping", joinColumns = @JoinColumn(name = "song_id", referencedColumnName = "song_id"), inverseJoinColumns = @JoinColumn(name = "album_id", referencedColumnName = "album_id"))
	private Album album;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "rating")
	private int rating;

	@Column(name = "artists")
	private String artist;

	@Column(name = "composer")
	private String composer;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinTable(name = "genre_songs_mapping", joinColumns = @JoinColumn(name = "song_id", referencedColumnName = "song_id"), inverseJoinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "genre_id"))
	private Genre genre;

	public Song(String title, Album album, int rating, String artist,
			String composer, Genre genre) {
		this.album = album;
		this.title = title;
		this.rating = rating;
		this.artist = artist;
		this.composer = composer;
		this.genre = genre;
	}

	public Song(String title, Album album, int rating, String artist,
			String composer, Genre genre, int songId) {
		this.album = album;
		this.title = title;
		this.rating = rating;
		this.artist = artist;
		this.composer = composer;
		this.genre = genre;
		this.songId = songId;
	}

	public Song() {

	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public static Song getSongMetadata(String location) throws IOException,
			SAXException, TikaException

	{
		String audioFileLoc = location;

		InputStream input = new FileInputStream(new File(audioFileLoc));
		ContentHandler handler = new DefaultHandler();
		Metadata metadata = new Metadata();
		Parser parser = new Mp3Parser();
		ParseContext parseCtx = new ParseContext();
		parser.parse(input, handler, metadata, parseCtx);
		input.close();
		String title = metadata.get("xmpDM:title");
		if (title == null) {
			Path path = FileSystems.getDefault().getPath(location);
			title = path.getFileName().toString();
		}
		String artist = metadata.get("xmpDM:artist");

		String genreName = metadata.get("xmpDM:genre");
		Genre genre = new Genre();
		genre.setGenreName(genreName);

		String composer = metadata.get("xmpDM:composer");

		String albumName = metadata.get("xmpDM:album");
		Album album = new Album();
		album.setAlbumName(albumName);

		int rating = 0;

		Song song = new Song(title, album, rating, artist, composer, genre);

		return song;
	}

	public int getSongId() {
		return songId;
	}

	public void setSongId(int songId) {
		this.songId = songId;
	}

	@Override
	public String toString() {
		return this.title;
	}

	/*
	 * @Override public boolean equals(Object song){ if(song == null) return
	 * false; if(!(song instanceof Song)) return false;
	 * 
	 * Song otherSong = (Song) song; if(this.title.equals(otherSong.title))
	 * return false; if(! this.artist.equals(otherSong.artist)) return false;
	 * if(! this.album.equals(otherSong.album)) return false; if(!
	 * this.composer.equals(otherSong.composer)) return false; if(!
	 * (this.rating==otherSong.rating)) return false; if(!
	 * this.genre.equals(otherSong.genre)) return false; return true; }
	 */

	@Override
	public int hashCode() {
		return (int) this.title.hashCode() * this.album.hashCode()
				* this.genre.hashCode() * this.artist.hashCode() * this.rating
				* this.composer.hashCode();
	}
}
