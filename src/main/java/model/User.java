package model;

import java.util.List;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

public class User {

	
	private Login login;
	
	@ManyToMany
	@JoinTable(name = "user_songs_mapping", joinColumns = @JoinColumn(name = " user_id", referencedColumnName = "user_id"), inverseJoinColumns = @JoinColumn(name = "song_id", referencedColumnName = "song_id"))
	private List<Song> songs;

	

	public User(String name, String emailId, Login login) {
		this.login = login;
	}

	public User() {

	}

	
	public User(List<Song> songs){
		this.songs = songs;
	}
	

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	
	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

}
