package model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "albums")
public class Album {

	@Id
	@GeneratedValue
	@Column(name = "album_id")
	private int albumId;
	
	@Column(name = "album_name", length = 250)
	private String albumName;
	
	@ManyToMany(cascade=CascadeType.ALL) 
	@JoinTable(name = "albums_songs_mapping", joinColumns = @JoinColumn(name = "album_id", referencedColumnName = "album_id"), inverseJoinColumns = @JoinColumn(name = "song_id", referencedColumnName = "song_id"))
	private List<Song> songs;

	public Album(){
		
	}
	
	public Album(String albumName){
		this.albumName = albumName;
	}
	
	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String toString() {
		return this.albumName;
	}

	@Override
	public boolean equals(Object album) {
		if (album == null)
			return false;
		if (!(album instanceof Album))
			return false;
		
		Album otherAlbum = (Album) album;
		
		if(! this.albumName.equals(otherAlbum)) return false;
		return true;
	}
	
	@Override
	public int hashCode(){
		return (int) this.albumName.hashCode();
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

}
