
package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import exceptions.InvalidUsernamePasswordException;

@Entity
@Table(name = "login", uniqueConstraints = {@UniqueConstraint(columnNames = {"user_name"})})
public class Login {
	
	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private int userId;
	
	@Column(name = "user_name", nullable=false )
	private String userName;
	
	@Column(name = "password", nullable=false )
	private String password;
	
	
	@OneToOne(cascade=CascadeType.ALL)
	   @JoinTable(name="user_roles",
	       joinColumns = {@JoinColumn(name="user_id", referencedColumnName="user_id")},
	       inverseJoinColumns = {@JoinColumn(name="role_id", referencedColumnName="id")}
	   )
	private Role role;
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@ManyToMany
	@JoinTable(name = "user_songs_mapping", joinColumns = @JoinColumn(name = " user_id", referencedColumnName = "user_id"), inverseJoinColumns = @JoinColumn(name = "song_id", referencedColumnName = "song_id"))
	private List<Song> songs;

	
	public static Login getLoginByUserId(int userId){
		return null;
	}
	
	public Login() {

	}
	

	public Login(String userName, String password,Role role)
			throws InvalidUsernamePasswordException {
		if (!userName.isEmpty() && userName != null && !password.isEmpty()
				&& password != null) {
			this.userName = userName;
			this.password = password;
			this.role = role;
		} else {
			throw new InvalidUsernamePasswordException(
					"username or password invalid");
		}
	}

	public Login(String userName, String password, int userId) {
		this.userName = userName;
		this.password = password;
		this.userId = userId;
		
	}

	public String getUserName() {
		System.out.println(this.userName);
		return this.userName;
	}

	public void setUserName(String userName)
			throws InvalidUsernamePasswordException {
		if (!userName.isEmpty() && userName != null) {
			this.userName = userName;
		} else {
			throw new InvalidUsernamePasswordException("Invalid username");
		}

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password)
			throws InvalidUsernamePasswordException {
		if (!password.isEmpty() && password != null) {
			this.password = password;
		} else {
			throw new InvalidUsernamePasswordException("Invalid password");
		}
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
	
	public void addSong(Song song){
		this.songs.add(song);
	}

}
