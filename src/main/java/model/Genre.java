package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "genre")
public class Genre {

	@Id
	@GeneratedValue
	@Column(name = "genre_id")
	private int genreId;
	
	@Column(name = "genre_name", length = 250)
	private String genreName;
	
	@ManyToMany(cascade=CascadeType.ALL) 
	@JoinTable(name = "genre_songs_mapping", joinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "genre_id"), inverseJoinColumns = @JoinColumn(name = "song_id", referencedColumnName = "song_id"))
	private List<Song> songs;
	
	
	public Genre(){
		
	}
	
	public Genre(String genreName){
		this.genreName = genreName;
	}

	public int getGenreId() {
		return genreId;
	}

	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.genreName;
	}

	@Override
	public boolean equals(Object genre) {
		if (genre == null)
			return false;
		if (!(genre instanceof Genre))
			return false;

		Genre otherGenre = (Genre) genre;

		if (!this.genreName.equals(otherGenre))
			return false;
		return true;
	}
	
	@Override
	public int hashCode(){
		return (int) this.genreName.hashCode();
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
}
