package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Library implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*public static final String REFERENCE_SONGS = "library.songs";*/
	private ArrayList<Song> songs = new ArrayList<Song>();

	public void add(Song song) {
		songs.add(song);

	}

	public void add(int index, Song song) {
		songs.add(index, song);
	}

	/*@JsonManagedReference(REFERENCE_SONGS)*/
	public ArrayList<Song> getSongs() {
		return songs;
	}

	public void setSongs(ArrayList<Song> songs) {
		this.songs = songs;
	}

	public void delete(Song song) {
		songs.remove(song);
	}

	public boolean isEmpty() {

		boolean flag = false;

		if (songs.isEmpty()) {
			flag = true;
		}
		return flag;
	}

	public int size() {
		return songs.size();

	}

	public int indexOf(Song song) {
		return songs.indexOf(song);
	}

	public Song getSongAt(int index) {
		return songs.get(index);
	}

	@Override
	public String toString() {

		String songArray = "[";
		for (Song song : this.getSongs()) {
			songArray = songArray + "{ \"title\":" + "\"" + song.getTitle()
					+ "\"," + "\"album\":" + "\"" + song.getAlbum().getAlbumName() + "\",\"artist\":"
					+ "\"" + song.getArtist() + "\",\"rating\":" + "\"" + song.getRating() + "\","
					+ "\"composer\":" + "\"" + song.getComposer() + "\"" +  "," + "\"genre\":"
					+ "\"" + song.getGenre().getGenreName() + "\"},";
		}
		songArray = songArray.substring(0, songArray.length() - 1);
		songArray = songArray + "]";
		return songArray;

	}
}
